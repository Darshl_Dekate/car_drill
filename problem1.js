function problem1(inventory, id) {
    var carDetail = [];
    if (Array.isArray(inventory)) {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id == id) {
                carDetail.push(inventory[index]);
            }
        }
    }
    else {
        return [];
    }
    if (carDetail !== undefined) {
        return carDetail;
    }
    else {
        return [];
    }
}
module.exports = problem1;